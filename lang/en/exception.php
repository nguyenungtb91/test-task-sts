<?php
return [
    /*Standard HTTP exception message*/
    'server_error' => 'Internal Server Error',
    'bad_request' => 'Bad Request',
    'unauthorized' => 'Unauthorized',
    'forbidden' => 'Forbidden',
    'not_found' => 'Not found',
    'unprocessable_entity' => 'Unprocessable Entity',
    'method_not_allowed' => 'Method Not Allowed',

    /*Custom exception message*/
    'api_error' => 'API error.',
    'missing_api_config' => 'Missing API config.',
    'data_source_not_support' => 'Data source not supported.'
];
