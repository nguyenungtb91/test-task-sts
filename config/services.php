<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Exchange rates module
    |--------------------------------------------------------------------------
    |
    | Get currency exchange rate data from Exchagerate API https://exchangeratesapi.io/
    | Note that Exchagerate API now is an APILayer product.
    | Endpoint API is https://api.apilayer.com instead "https://api.exchangeratesapi.io/v1/"
    | Official guideline: https://apilayer.com/marketplace/exchangerates_data-api
    |
    | The api_key is the authentication token issued by APILayer.
    | All requests made to the API must hold a custom HTTP header named "apikey"
    | broker_fee_per_day: Broker fees are calculated on a daily basis, deducted directly from revenue when analyzing
    | maximum_date_range: Maximum date range allowed for user input for analysis
    | allow_base_currency and allow_exchange_currency: base currency and currencies to be analyzed
    |
    */
    'exchange_rates_api' => [
        'api_url' => env('EXCHANGE_RATES_API_URL', 'https://api.apilayer.com/exchangerates_data/timeseries'),
        'api_key' => env('EXCHANGE_RATES_API_KEY'),
        'broker_fee_per_day' => env('EXCHANGE_RATES_BROKER_FEE', 1),
        'maximum_date_range' => env('EXCHANGE_RATES_MAX_DATE_RANGE', 60),
        'allow_base_currency' => 'USD',
        'allow_exchange_currency' => ['RUB', 'GBP', 'JPY', 'EUR']
    ],

];
