# Exchange Rate Analysis

## Required

List environment need to run project:

- PHP >= 8.0.2
- MySQL 8.0

## Architecture Overview

### Packaging Rules

List components in source code and explain it.

```
.
├── app - contains the core code
│      ├── Console - contains all of the custom commands
│      ├── Exceptions - contains all exception handler
│      ├── Http
│      │      ├── Controllers - handle requests logic
│      │      │             └── Auth - authentication logic
│      │      ├── Middleware - contains middleware mechanism, comprising the filter mechanism and communication between response and request
│      │      └── Requests - contains validation request logic
│      ├── Models - contains all of entities.
│      ├── Providers - Laravel application bootstrapping.
│      ├── Repositories - the layer of abstraction between the domain and data mapping layers
│      ├── Services - contains application logic function
│      ├── Solutions - custom module
│      └── Traits - contains all of the Trait class for application
├── bootstrap - contains all the application bootstrap scripts
├── config - contains various configurations and associated parameters required for the application
├── database - contains database migrates, seeder, factory
├── docs - contains all of documents explain about system or source code
├── lang - contains localization files
├── node_modules - contains all downloaded packages from npm
├── public
├── resources
├── routes - contains all of the route definitions for application
├── storage
│        └── logs - Logging
├── tests
│    ├── Feature - Feature test
│    └── Unit - Unit test
└── vendor - contains all Composer dependencies.

```

## Rules

Define rules for steps of process as naming convention, filename convention...
Can split separate file, link to it

## Documents

### Database
[Database Design](./docs/database/erd.md)

## Get started

### Set up `app`

- Clone repository:
```console
git clone git@gitlab.com:nguyenungtb91/test-task-sts.git
```

- Create .env file from .env.example
```
cp .env.example .env
```
- Set your exchangeratesapi.io api key into EXCHANGE_RATES_API_KEY
- Install application's dependencies
```console
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
```
- Run docker via Laravel `sail`
```console
./vendor/bin/sail up -d
```
- Enter the test_task_app container
```console
./vendor/bin/sail root-shell
```
#### *All bellow commands need run into test_task_app container*
- Install application's javascript dependencies
```console
npm install
```
- Generate application key
```console
php artisan key:generate
```
- Create database and initialize database with sample data
```console
php artisan migrate
php artisan db:seed
```
- Run Vite
```console
# Run the Vite development server...
npm run dev
```
- Stop app
```console
./vendor/bin/sail down
```

## App Server

Open http://localhost

Sample first user:

- username: `johndoe`
- password: `123456`

## DB (PhpMyAdmin)

Open http://localhost:8080/
## [Thrid-party service]
[Laravel 5 Repositories](https://github.com/andersao/l5-repository)

## Security Vulnerabilities
If you discover a security vulnerability within this project, please send an e-mail to Nguyen Ung via [nguyenungtb91@gmail.com](mailto:nguyenungtb91@gmail.com). All security vulnerabilities will be promptly addressed.

## License
This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Application's Screenshot
![Login](./docs/screenshots/login.png "Login screen")*Login screen*

![Dashboard](./docs/screenshots/dashboard.png "Dashboard screen")*Dashboard screen*

![Dashboard with result](./docs/screenshots/dashboard-with-result.png "Dashboard screen with analysis result")*Dashboard screen with analysis result*
