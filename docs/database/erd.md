```plantuml
@startuml
hide circle
skinparam linetype ortho
title Exchange Rates Entity Relationship Diagram

'Entity
entity "currencies" {
  *id: number <<generated>>
  --
  *name: string
  *abbrev: string
  *symbol: string
}

entity "exchange_rates" {
  *id: number <<generated>>
  --
  *date
  *from_currency_id
  *to_currency_id
  *value
  created_at
  updated_at
}


'Relationship
currencies ||......|{ exchange_rates
currencies ||......|{ exchange_rates
@enduml
```
