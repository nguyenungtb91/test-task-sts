<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('users')->upsert([
            'name' => 'John Doe',
            'username' => 'johndoe',
            'email' => 'johndoe@example.net',
            'password' => Hash::make('123456'),
        ], 'email');
    }
}
