<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('currencies')->upsert([
            ['name' => 'US Dollar', 'abbrev' => 'USD', 'symbol' => '$'],
            ['name' => 'Russian Ruble', 'abbrev' => 'RUB', 'symbol' => '₽'],
            ['name' => 'Euro', 'abbrev' => 'EUR', 'symbol' => '€'],
            ['name' => 'Pound sterling', 'abbrev' => 'GBP', 'symbol' => '$'],
            ['name' => 'Japanese yen', 'abbrev' => 'JPY', 'symbol' => '¥'],
        ], 'abbrev');
    }
}
