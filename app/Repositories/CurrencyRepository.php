<?php
namespace App\Repositories;

use App\Models\Currency;
use App\Repositories\Interfaces\CurrencyRepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;

class CurrencyRepository extends BaseRepository implements CurrencyRepositoryInterface {
    /**
     * @return string
     */
    public function model(): string
    {
        return Currency::class;
    }
}
