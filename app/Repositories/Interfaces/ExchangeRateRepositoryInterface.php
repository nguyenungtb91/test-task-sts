<?php
namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;

interface ExchangeRateRepositoryInterface extends RepositoryInterface {
    /**
     * Determines all days with full exchange rate data for a given date range.
     * For example, check with 4 currencies, days with only 3 types of exchanges will be disqualified
     *
     * @param int $baseId
     * @param array $exchangesId
     * @param string $startDate
     * @param string $endDate
     * @return Builder
     */
    public function getChunkDatesHaveFullData(int $baseId, array $exchangesId, string $startDate, string $endDate): Builder;

    /**
     * Get exchange rate by base currency, list currency and date range
     *
     * @param int $baseId
     * @param array $exchangesId
     * @param string $startDate
     * @param string $endDate
     * @return Builder
     */
    public function getByCurrencyAndDateRange(int $baseId, array $exchangesId, string $startDate, string $endDate): Builder;
}
