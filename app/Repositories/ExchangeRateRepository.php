<?php
namespace App\Repositories;

use App\Models\ExchangeRate;
use App\Repositories\Interfaces\ExchangeRateRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;

class ExchangeRateRepository extends BaseRepository implements ExchangeRateRepositoryInterface {
    /**
     * @return string
     */
    public function model(): string
    {
        return ExchangeRate::class;
    }

    /**
     * @inheritDoc
     */
    public function getChunkDatesHaveFullData(int $baseId, array $exchangesId, string $startDate, string $endDate): Builder
    {
        return $this->model->select(DB::raw('date, from_currency_id, SUM(1) AS count_currency'))
            ->where('from_currency_id', $baseId)
            ->whereIn('to_currency_id', $exchangesId)
            ->where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->groupBy('from_currency_id', 'date')
            ->having('count_currency', count($exchangesId))
            ->orderBy('date', 'ASC');
    }

    /**
     * @inheritDoc
     */
    public function getByCurrencyAndDateRange(int $baseId, array $exchangesId, string $startDate, string $endDate): Builder
    {
        return $this->model->where('from_currency_id', $baseId)
            ->whereIn('to_currency_id', $exchangesId)
            ->where('date', '>=', $startDate)
            ->where('date', '<=', $endDate)
            ->orderBy('date', 'ASC');
    }
}
