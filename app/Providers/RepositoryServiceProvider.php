<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $repositories = [
            [
                \App\Repositories\Interfaces\CurrencyRepositoryInterface::class,
                \App\Repositories\CurrencyRepository::class,
            ],
            [
                \App\Repositories\Interfaces\ExchangeRateRepositoryInterface::class,
                \App\Repositories\ExchangeRateRepository::class,
            ],
            [
                \App\Repositories\Interfaces\UserRepositoryInterface::class,
                \App\Repositories\UserRepository::class,
            ]
        ];

        foreach ($repositories as $repository) {
            $this->app->bind($repository[0], $repository[1]);
        }
    }
}
