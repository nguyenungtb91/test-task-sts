<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SolutionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $solutions = [
            [
                \App\Solutions\ExchangeRates\Interfaces\ExchangeRateInterface::class,
                \App\Solutions\ExchangeRates\Classes\ExchangeRateAnalysis::class,
            ],
        ];

        foreach ($solutions as $item) {
            $this->app->bind($item[0], $item[1]);
        }
    }
}
