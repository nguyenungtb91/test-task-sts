<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $services = [
            [
                \App\Services\Interfaces\ExchangeRateServiceInterface::class,
                \App\Services\ExchangeRatesService::class,
            ],
        ];

        foreach ($services as $service) {
            $this->app->bind($service[0], $service[1]);
        }
    }
}
