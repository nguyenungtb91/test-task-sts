<?php
namespace App\Traits;

use Exception;
use Illuminate\Http\Request;

trait LoggingTrait
{
    /**
     * Collect base information from request like uri, method, ip, etc.
     *
     * @param null $request
     * @return array
     */
    public function collectBaseInformation($request = null) : array
    {
        try {
            // Check request come from user call API
            if ($request instanceof Request) {
                $header = $request->header();
                $result = [
                    'type'       => 'USER_REQUEST',
                    'uri'        => $request->getUri() ?? null,
                    'method'     => $request->getMethod() ?? null,
                    'referer'    => $request->headers->get('referer') ?? null,
                    'user-agent' => $header['user-agent'] ?? null,
                    'parameters' => $request->except(
                        'password',
                        'new_password',
                        'confirm_password',
                        'current_password'
                    ),
                ];
                $user = $request->user();
                if (isset($user)) {
                    $result['user_id'] = $user->id;
                }
                return $result;
            }
            // Check request come from system command like "php artisan ..."
            if ($_SERVER['SCRIPT_NAME'] === 'artisan') {
                return [
                    'type'    => 'SYSTEM_COMMAND',
                    'command' => $_SERVER['argv'] ?? null,
                ];
            }
        } catch (Exception $error) {
            // Do nothing, just prevent case some error occur
        }
        return ['type' => 'UNKNOWN'];
    }
}
