<?php
namespace App\Traits;

use Exception;
use Illuminate\Support\Facades\Log;

trait HandleExceptionTrait
{
    use LoggingTrait;

    /**
     * Handle error threw in catch block
     * Simple call this function: $this->handleException($e);
     * @param Exception $e
     * @param object|null $request
     * @param array $logs
     */
    public function handleException(
        Exception $e,
        object $request = null,
        array $logs = []
    ): void
    {
        $logChannel = 'errorlog';
        $this->collectRequestInformation($request, $logs);
        $this->addErrorLogInformation($e, $logs);
        $this->writeLogToLoggingChannel($logChannel, $logs);
    }

    protected function collectRequestInformation($request, array &$logs): void
    {
        if (!isset($logs['request'])) {
            $logs['request'] = $this->collectBaseInformation($request);
        }
    }

    /**
     * @param Exception $e
     * @param array $logs
     * @return void
     */
    protected function addErrorLogInformation(Exception $e, array &$logs): void
    {
        $logs['error'] = [
            'message' => $e->getMessage(),
            'file'    => $e->getFile(),
            'line'    => $e->getLine(),
            'trace'   => $this->traceFewLastAction($e, 4),
        ];
    }

    /**
     * @param $e
     * @param $stepNum
     * @return string[]
     */
    protected function traceFewLastAction($e, $stepNum) : array
    {
        try {
            return $this->minimizeTraces($e->getTrace(), $stepNum);
        } catch (Exception $error) {
            return ["Trace error"];
        }
    }

    /**
     * @param array $traces
     * @param int $step
     * @return array
     */
    protected function minimizeTraces(array $traces, int $step = -1) : array
    {
        $result = [];
        foreach ($traces as $trace) {
            if (count($result) == $step) {
                break;
            }
            $fileOrClassName = '';
            $isSystemCall    = true;
            if (isset($trace['class']) && $this->isNotSystemName($trace['class'])) {
                $fileOrClassName = $this->getClassName($trace['class']);
                $isSystemCall = false;
            }
            if (isset($trace['file']) && $this->isNotSystemName($trace['file'])) {
                $fileOrClassName = $this->getFileName($trace['file']);
                $isSystemCall = false;
            }
            if (isset($trace['function']) && $this->isNotAnonymousFunction($trace['function']) && !$isSystemCall) {
                $result[] = $fileOrClassName . "." . $trace['function'] . "()";
            }
        }
        return $result;
    }

    /**
     * @param $string
     * @return bool
     */
    protected function isNotSystemName($string) : bool
    {
        return str_contains($string, 'App\\') || str_contains($string, 'App\/') || str_contains($string, 'App/');
    }

    /**
     * @param $string
     * @return bool
     */
    protected function isNotAnonymousFunction($string) : bool
    {
        return !str_contains($string, '{closure}');
    }

    /**
     * @param $string
     * @return string
     */
    protected function detectSeparator($string) : string
    {
        if (str_contains($string, '\\')) {
            return '\\';
        }
        if (str_contains($string, '\/')) {
            return '\/';
        }
        if (str_contains($string, '/')) {
            return '/';
        }
        return '';
    }

    /**
     * @param string $string
     * @return bool|string
     */
    protected function getClassName(string $string): bool|string
    {
        $arrayDir = explode($this->detectSeparator($string), $string);
        return end($arrayDir);
    }

    /**
     * @param string $string
     * @return bool|string
     */
    protected function getFileName(string $string): bool|string
    {
        $arrayDir = explode($this->detectSeparator($string), $string);
        $file     = end($arrayDir);
        $arrFileAndExtension = explode('.', $file);
        return reset($arrFileAndExtension);
    }

    /**
     * Json encode log and write to logging channel
     *
     * @param string $channel
     * @param array $logs
     */
    protected function writeLogToLoggingChannel(string $channel, array $logs): void
    {
        Log::channel($channel)->error(json_encode($logs));
    }
}
