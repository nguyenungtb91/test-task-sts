<?php
namespace App\Solutions\ExchangeRates\Interfaces;

interface ExchangeRateDataReaderInterface
{
    /**
     * Get exchange rates data from data source (API, DB,...)
     * All rates value must be round to float with 2 digits after comma
     * Example data format:
     * [
     *  'rates' => [
     *     "2022-12-01" => [
     *        "GBP" => 0.81,
     *        "EUR" => 0.94,
     *     ],
     *   ],
     *  'formatted_rates' => [
     *      "GBP" => [
     *         "2022-12-01" => 0.81,
     *      ],
     *      "EUR" => [
     *         "2022-12-01" => 0.94,
     *      ],
     *   ]
     * ]
     *
     * @param string $from
     * @param string|array $to
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function exchangeRateBetweenDateRange(string $from, string|array $to, string $startDate, string $endDate): array;
}
