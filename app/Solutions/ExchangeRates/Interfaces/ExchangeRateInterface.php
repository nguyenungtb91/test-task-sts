<?php
namespace App\Solutions\ExchangeRates\Interfaces;

interface ExchangeRateInterface
{
    /**
     * Main function get data, analyze, process cache and make the best choice
     * - Determinate data source: directly get from api or get from DB
     * - Get and format data
     * - Save data to DB if needed
     * - Calculate and return best choice
     *
     * @param string $from
     * @param string|array $to
     * @param string $startDate
     * @param string $endDate
     * @param int $baseMoney
     * @return array
     */
    public function analysisExchangeRates(
        string $from,
        string|array $to,
        string $startDate,
        string $endDate,
        int $baseMoney
    ): array;

    /**
     * Calculator and return best choice: which currency to buy, buy date, sell date and revenue
     * Example return data:
     * [
     *   'currency' => 'RUB',
     *   'maxEarn' => 125.94,
     *   'startDate' => '2022-12-01',
     *   'endDate' => '2022-12-15',
     *   'buyValue' => 67.85,
     *   'sellValue' => 65.31
     * ]
     *
     * @param array $array
     * @param int $baseMoney
     * @return array
     */
    public function getBestChoice(array $array, int $baseMoney): array;
}
