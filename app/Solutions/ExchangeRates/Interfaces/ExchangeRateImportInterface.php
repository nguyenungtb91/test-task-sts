<?php
namespace App\Solutions\ExchangeRates\Interfaces;

interface ExchangeRateImportInterface
{
    /**
     * Import exchange rate to database to cache
     *
     * @param array $exchangeRateData
     * @param int $baseCurrencyId
     * @param array $currencyIdList
     * @return void
     */
    public function import(array $exchangeRateData, int $baseCurrencyId, array $currencyIdList): void;
}
