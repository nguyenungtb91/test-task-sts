<?php
namespace App\Solutions\ExchangeRates\Classes;

use App\Solutions\ExchangeRates\Interfaces\ExchangeRateDataReaderInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class ExchangeRateReaderFactory
{
    /**
     * @var array|string[]
     */
    private array $instanceMapping = [
        'exchange_rates_api' => ExchangeRateApiReader::class,
        'exchange_rates_db' => ExchangeRateDBReader::class,
    ];

    /**
     * Load classes that implement ExchangeRateDataReaderInterface based on allow instance type
     *
     * @param string $sourceType
     * @return ExchangeRateDataReaderInterface
     */
    public function loadInstance(string $sourceType): mixed
    {
        if (!isset($this->instanceMapping[$sourceType])) {
            throw new BadRequestException(trans('exception.data_source_not_support'));
        }
        return app($this->instanceMapping[$sourceType]);
    }
}
