<?php

namespace App\Solutions\ExchangeRates\Classes;

use App\Repositories\Interfaces\ExchangeRateRepositoryInterface;
use App\Solutions\ExchangeRates\Interfaces\ExchangeRateDataReaderInterface;

class ExchangeRateDBReader implements ExchangeRateDataReaderInterface
{
    public function __construct(protected ExchangeRateRepositoryInterface $repository)
    {
    }

    /**
     * Get and format the exchange rates data from the exchange_rates table
     *
     * @inheritDoc
     */
    public function exchangeRateBetweenDateRange(string $from, string|array $to, string $startDate, string $endDate): array {
        $exchangeRateFromDB = [
            'rates' => [],
            'formatted_rates' => []
        ];
        if (is_string($to)) {
            $to = [$to];
        }
        $dataFromDb = $this->repository
            ->getByCurrencyAndDateRange($from, $to, $startDate, $endDate)
            ->get();
        foreach ($dataFromDb as $row) {
            $exchangeRateFromDB['rates'][$row->date][$row->exchangeCurrency->abbrev] = (float)$row->value;
            $exchangeRateFromDB['formatted_rates'][$row->exchangeCurrency->abbrev][$row->date] = (float)$row->value;
        }
        return $exchangeRateFromDB;
    }
}
