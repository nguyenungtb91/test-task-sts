<?php

namespace App\Solutions\ExchangeRates\Classes;

use App\Repositories\Interfaces\ExchangeRateRepositoryInterface;
use App\Solutions\ExchangeRates\Interfaces\ExchangeRateImportInterface;
use Illuminate\Support\Facades\Http;
use App\Solutions\ExchangeRates\Interfaces\ExchangeRateDataReaderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use LogicException;

class ExchangeRateApiReader implements ExchangeRateDataReaderInterface, ExchangeRateImportInterface
{
    private string $baseUrl;

    private string $apiKey;

    public function __construct(protected ExchangeRateRepositoryInterface $repository,)
    {
        $this->baseUrl = config('services.exchange_rates_api.api_url');
        $this->apiKey = config('services.exchange_rates_api.api_key');
    }

    /**
     * @inheritDoc
     */
    public function exchangeRateBetweenDateRange(string $from, string|array $to, string $startDate, string $endDate): array {
        if (empty($this->apiKey) || $this->baseUrl) {
            throw new LogicException(trans('exception.missing_api_config'));
        }
        $response = Http::withHeaders([
            "Content-Type" => "text/plain",
            "apikey" => $this->apiKey
        ])->get($this->baseUrl, [
            'end_date' => $endDate,
            'start_date' => $startDate,
            'base' => $from,
            'symbols' => is_array($to) ? implode(',', $to) : $to
        ]);
        if ($response->status() != Response::HTTP_OK) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, trans('exception.api_error'));
        }
        $responseArr = $response->json();

        $formattedRate = [];
        $exchangeRates = $responseArr['rates'];
        $tmpCollection = collect($exchangeRates);
        $tmpCollection->map(function ($item, $date) use (&$formattedRate, &$exchangeRates) {
            foreach ($item as $currency => $value) {
                $roundValue = round($value, 2);
                $exchangeRates[$date][$currency] = $roundValue;
                $formattedRate[$currency][$date] = $roundValue;
            }
        });

        return [
            'rates' => $exchangeRates,
            'formatted_rates' => $formattedRate
        ];
    }

    /**
     * @inheritDoc
     */
    public function import(array $exchangeRateData, int $baseCurrencyId, array $currencyIdList): void
    {
        $dataUpInsert = [];
        foreach ($exchangeRateData as $date => $detail) {
            foreach ($detail as $currency => $value) {
                $dataUpInsert[] = [
                    'date' => $date,
                    'from_currency_id' => $baseCurrencyId,
                    'to_currency_id' => $currencyIdList[$currency],
                    'value' => $value,
                ];
            }
        }
        if (!empty($dataUpInsert)) {
            $this->repository->upsert($dataUpInsert, ['date', 'from_currency_id', 'to_currency_id'], ['value', 'created_at']);
        }
    }
}
