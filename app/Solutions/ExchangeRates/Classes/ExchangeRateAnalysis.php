<?php
namespace App\Solutions\ExchangeRates\Classes;

use App\Repositories\Interfaces\CurrencyRepositoryInterface;
use App\Repositories\Interfaces\ExchangeRateRepositoryInterface;
use App\Solutions\ExchangeRates\Interfaces\ExchangeRateInterface;
use Carbon\Carbon;
use LogicException;

class ExchangeRateAnalysis implements ExchangeRateInterface
{
    public function __construct(
        protected ExchangeRateReaderFactory       $exchangeRateReaderResolver,
        protected ExchangeRateRepositoryInterface $repository,
        protected CurrencyRepositoryInterface     $currencyRepository,
    ) {}

    /**
     * @inheritDoc
     */
    public function analysisExchangeRates($from, $to, $startDate, $endDate, $baseMoney): array
    {
        $currencyIdList = $this->getListCurrencyIdByAbbrev($to);
        $baseCurrency = $this->currencyRepository->findByField('abbrev', $from)->firstOrFail();
        $baseCurrencyId = $baseCurrency->id;

        $determinateSourceGet = $this->determinateSourceGet($baseCurrencyId, $currencyIdList, $startDate, $endDate);
        $exchangeRateFromApi = [];
        $exchangeRateFromDB = [];

        if (!empty($determinateSourceGet['dateWouldGetFromDB'])) {
            $dataReader = $this->exchangeRateReaderResolver->loadInstance('exchange_rates_db');
            $exchangeRateFromDB = $dataReader->exchangeRateBetweenDateRange($baseCurrencyId, $currencyIdList, $startDate, $endDate);
        }
        if (!empty($determinateSourceGet['dateWouldFetchFromApi'])) {
            $dataReader = $this->exchangeRateReaderResolver->loadInstance('exchange_rates_api');
            $startDateGetApi = reset($determinateSourceGet['dateWouldFetchFromApi']);
            $endDateGetApi = end($determinateSourceGet['dateWouldFetchFromApi']);
            $exchangeRateFromApi = $dataReader->exchangeRateBetweenDateRange($from, $to, $startDateGetApi, $endDateGetApi);
            if (!empty($exchangeRateFromApi['rates'])) {
                $dataReader->import($exchangeRateFromApi['rates'], $baseCurrencyId, $currencyIdList);
            }
        }

        $exchangeRates = array_merge_recursive($exchangeRateFromApi, $exchangeRateFromDB);
        $bestChoice = $this->getBestChoice($exchangeRates['formatted_rates'], $baseMoney);

        return [
            'start_date' => $startDate,
            'end_date' => $endDate,
            'rates' => $exchangeRates['rates'],
            'best_choice' => [
                'best_currency' => $bestChoice['currency'],
                'base_currency' => $from,
                'base_money' => $baseMoney,
                'buy_date' => $bestChoice['startDate'],
                'buy_value' => $bestChoice['buyValue'],
                'sell_date' => $bestChoice['endDate'],
                'sell_value' => $bestChoice['sellValue'],
                'earn_money' => number_format($bestChoice['maxEarn'], 2),
                'revenue' => number_format($bestChoice['maxEarn'] - $baseMoney, 2),
                'fee_per_day' => config('services.exchange_rates_api.broker_fee_per_day'),
                'date_count' => Carbon::parse($bestChoice['endDate'])->diffInDays(Carbon::parse($bestChoice['startDate']))
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function getBestChoice(array $array, int $baseMoney): array
    {
        $maxPerCurrency = [];
        foreach ($array as $currency => $exchangeRateData) {
            $exchangeRates = array_values($exchangeRateData);
            $exchangeDates = array_keys($exchangeRateData);

            $count = count($exchangeRates);
            $feePerDay = config('services.exchange_rates_api.broker_fee_per_day');
            $maxArr = [];
            if ($count == 1) {
                throw new LogicException('Please choose period more than 2 day');
            }
            for ($i = 0; $i < $count - 1; $i ++) {
                if ($count == 2 && $i == 1) {
                    break;
                }
                $maxVal = ($exchangeRates[$i] / $exchangeRates[$i+1]) * $baseMoney - $feePerDay;
                $startIndex = $i;
                $endIndex = $i + 1;
                if ($i < $count - 2) {
                    for ($j = $i + 2; $j < $count; $j ++) {
                        if ($j >= $count-1) {
                            continue;
                        }
                        $tmpMaxVal = ($exchangeRates[$i] / $exchangeRates[$j]) * $baseMoney - ($j - $i) * $feePerDay;
                        if ($tmpMaxVal > $maxVal) {
                            $maxVal = $tmpMaxVal;
                            $endIndex = $j;
                        }
                    }
                }
                $maxArr[] = compact('maxVal', 'startIndex', 'endIndex');
            }

            $collectionMax = collect($maxArr);
            $best = $collectionMax->sortByDesc('maxVal')->first();

            $maxEarn = $best['maxVal'];
            $startDate = $exchangeDates[$best['startIndex']];
            $endDate = $exchangeDates[$best['endIndex']];
            $buyValue = $exchangeRates[$best['startIndex']];
            $sellValue = $exchangeRates[$best['endIndex']];

            $maxPerCurrency[] = compact([
                'currency', 'maxEarn', 'startDate', 'endDate', 'buyValue', 'sellValue'
            ]);
        }
        $overallCollection = collect($maxPerCurrency);
        $sortedDescByEarn = $overallCollection->sortByDesc('maxEarn');
        return $sortedDescByEarn->first();
    }

    /**
     * Based on date range, determine which date range to call API or get from DB.
     *
     * There is a trade-off between calling the api and getting from the DB.
     * Example, consider the case in the DB where data is available from 05 to 10/12.
     * - If you need analysis from 01 to 20/12, application will get all data from once time call API,
     *   instead split to 2 API request (01 to 04 and 11 to 20) and 1 time get from DB (05-12) .
     * - If you need to analyze from 01 to 08/12,
     *   application will get all data from API for date 01 to 04 and get it from DB for date 05-08.
     * - If you need analysis from 08 to 15/12,
     *   application will get all data from API for date 08 to 10 and get it from DB for date 11-15.
     *
     * @param $baseCurrencyId
     * @param $currencyIdList
     * @param $startDate
     * @param $endDate
     * @return array
     */
    private function determinateSourceGet($baseCurrencyId, $currencyIdList, $startDate, $endDate): array
    {
        $dateWouldGetFromDB = [];
        $dateHaveFullData = $this->getChunkDatesHaveFullData($baseCurrencyId, $currencyIdList, $startDate, $endDate);
        $inputArrayDate = $this->createArrayFromDates($startDate, $endDate);
        $splitArr = $this->splitArrayToSequencePart(array_keys($dateHaveFullData));
        if (count($splitArr) == 1) {
            if (reset($splitArr[0]) == $startDate || end($splitArr[0]) == $endDate) {
                $dateWouldFetchFromApi = array_diff($inputArrayDate, $splitArr[0]);
                $dateWouldGetFromDB = $splitArr[0];
            } else {
                $dateWouldFetchFromApi = $inputArrayDate;
            }
        } else {
            $dateWouldFetchFromApi = $inputArrayDate;
        }
        return compact('dateWouldFetchFromApi', 'dateWouldGetFromDB');
    }

    /**
     * @param array $abbrevs
     * @return array|null
     */
    private function getListCurrencyIdByAbbrev(array $abbrevs): ?array
    {
        return $this->currencyRepository->findWhereIn('abbrev', $abbrevs)->pluck('id', 'abbrev')->toArray();
    }

    /**
     * @param int $baseId
     * @param array $exchangesId
     * @param string $startDate
     * @param string $endDate
     * @return array|null
     */
    private function getChunkDatesHaveFullData(int $baseId, array $exchangesId, string $startDate, string $endDate): ?array
    {
        return $this->repository->getChunkDatesHaveFullData($baseId, $exchangesId, $startDate, $endDate)
            ->get()
            ->keyBy('date')
            ->toArray();
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return array
     */
    private function createArrayFromDates($startDate, $endDate): array
    {
        $dateArray = [];
        $currentDate = $startDate;
        while ($currentDate <= $endDate) {
            $dateArray[] = $currentDate;
            $currentDate = Carbon::parse($currentDate)->addDay()->format('Y-m-d');
        }
        return $dateArray;
    }

    /**
     * @param array $array
     * @return array
     */
    private function splitArrayToSequencePart(array $array): array
    {
        $arr = [];
        for ($i = 0; $i < count($array) - 1; $i++) {
            $nextDate = Carbon::parse($array[$i+1]);
            $currentDate = Carbon::parse($array[$i]);
            if ($nextDate->diffInDays($currentDate) != 1) {
                $arr[] = array_splice($array, 0, $i + 1);
                $i = 0;
            }
        }
        if (count($array) > 0) {
            $arr[] = $array;
        }
        return $arr;
    }
}
