<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetBestChoiceRequest;
use App\Services\Interfaces\ExchangeRateServiceInterface;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        public ExchangeRateServiceInterface $exchangeRateService
    ) {}

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $inspiringQuote = \Illuminate\Foundation\Inspiring::quote();
        return view('home', compact('inspiringQuote'));
    }

    /**
     * @param GetBestChoiceRequest $request
     * @return JsonResponse
     */
    public function analysisExchangeRates(GetBestChoiceRequest $request): JsonResponse
    {
        $params = $request->all();
        $result = $this->doRequest(function() use ($params) {
            return $this->exchangeRateService->analysisExchangeRates($params);
        }, $request);
        return $this->sendResponse($result);
    }
}
