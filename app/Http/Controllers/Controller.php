<?php

namespace App\Http\Controllers;

use App\Traits\HandleExceptionTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use HandleExceptionTrait;

    /**
     * @param $response
     * @param int $code
     * @return JsonResponse
     */
    public function sendResponse($response, int $code = Response::HTTP_OK): JsonResponse
    {
        return response()->json($response, $code);
    }

    /**
     * Handling requests with data changes and exception handling
     *
     * @param callable $callback
     * @param Request $request
     * @return array
     */
    protected function doRequest(callable $callback, Request $request): array
    {
        DB::beginTransaction();
        try {
            $results = call_user_func_array($callback, []);
            DB::commit();
            return $results;
        } catch (Exception $e) {
            DB::rollBack();
            $this->handleException($e, $request);
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, trans('exception.server_error'));
        }
    }

    /**
     * Handling requests without data changes and exception handling
     *
     * @param callable $callback
     * @param Request $request
     * @return mixed
     */
    protected function getData(callable $callback, Request $request): mixed
    {
        try {
            return call_user_func_array($callback, []);
        } catch (Exception $e) {
            $this->handleException($e, $request);
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, trans('exception.server_error'));
        }
    }
}
