<?php
/**
 * Validation handling for analysis exchange rates request
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetBestChoiceRequest extends FormRequest
{
    use ValidationTrait;

    /**
     * Prepare input data for validation
     * Convert date_range to start_date and end_date to validate
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $dateRangeInput = $this->request->get('date_range');
        if (!empty($dateRangeInput)) {
            $extract = explode(' to ', $dateRangeInput);
            if (!empty($extract[0])) {
                $this->merge(['start_date' => $extract[0]]);
            }
            if (!empty($extract[1])) {
                $this->merge(['end_date' => $extract[1]]);
            }
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'date_range' => [
                'required',
                //accept only format "Y-m-d to Y-m-d" from daterangepicker input via frontend page
                'regex: /^\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])( to )\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$/'
            ],
            'start_date' => [
                'required',
                'date_format:Y-m-d',
                'before:end_date'
            ],
            'end_date' => [
                'required',
                'date_format:Y-m-d',
                'before:tomorrow',
                'after:start_date'
            ],
            'amount' => 'required|integer|min:1',
        ];
    }

    /**
     * @return array|string[]
     */
    public function messages(): array
    {
        return [
            'start_date.before' => trans('validation.custom.invalid_date_range'),
            'start_date.*' => trans('validation.custom.invalid_date'),
            'end_date.before' => trans('validation.custom.invalid_date_range'),
            'end_date.*' => trans('validation.custom.invalid_date'),
        ];
    }
}
