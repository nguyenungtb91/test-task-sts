<?php
/**
 * Common validation handling
 */
namespace App\Http\Requests;

use App\Traits\HandleExceptionTrait;
use App\Traits\LoggingTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

trait ValidationTrait
{
    use HandleExceptionTrait, LoggingTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get single message as plain text from array of error messages and response json.
     * By default, throw Bad Request Exception if no message can be identified.
     * Overall, write all message into log file.
     *
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator): void
    {
        $allMessage = $validator->errors()->getMessages();
        $formFieldErrorMessages = [];
        foreach ($allMessage as $key => $item) {
            $formFieldErrorMessages[$key] = $item[0];
        }
        $this->logValidateErrorData($allMessage);
        if (!empty($formFieldErrorMessages)) {
            $formFieldErrorMessages = array_unique($formFieldErrorMessages);
            throw new HttpResponseException(
                response()->json(reset($formFieldErrorMessages), Response::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
        throw new HttpResponseException(
            response()->json([
                'message' => trans('exception.bad_request'),
            ], Response::HTTP_BAD_REQUEST)
        );
    }

    /**
     * Write error info into logfile with extra request info
     * @param $logs
     * @return void
     */
    private function logValidateErrorData($logs): void
    {
        $this->collectRequestInformation($this, $logs);
        $this->writeLogToLoggingChannel('errorlog', $logs);
    }
}
