<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ExchangeRate extends Model
{
    use HasFactory;

    /**
     * Get the replies for the comment.
     */
    public function exchangeCurrency(): HasOne
    {
        return $this->hasOne(Currency::class, 'id', 'to_currency_id');
    }
}
