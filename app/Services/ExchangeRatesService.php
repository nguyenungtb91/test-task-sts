<?php

namespace App\Services;

use App\Repositories\Interfaces\CurrencyRepositoryInterface;
use App\Repositories\Interfaces\ExchangeRateRepositoryInterface;
use App\Services\Interfaces\ExchangeRateServiceInterface;
use App\Solutions\ExchangeRates\Interfaces\ExchangeRateInterface;

class ExchangeRatesService extends BaseService implements ExchangeRateServiceInterface
{
    public function __construct(
        ExchangeRateRepositoryInterface $repository,
        public CurrencyRepositoryInterface $currencyRepository,
        public ExchangeRateInterface $exchangeRate
    ) {
        $this->repository = $repository;
    }

    /**
     * @inheritDoc
     */
    public function analysisExchangeRates(array $params): array
    {
        $from = config('services.exchange_rates_api.allow_base_currency');
        $to = config('services.exchange_rates_api.allow_exchange_currency');
        $startDate = $params['start_date'];
        $endDate = $params['end_date'];
        $baseMoney = $params['amount'];

        $calculateResult = $this->exchangeRate->analysisExchangeRates($from, $to, $startDate, $endDate, $baseMoney);

        return [
            'data' => $calculateResult,
            'success' => true
        ];
    }
}
