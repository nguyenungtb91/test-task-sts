<?php
namespace App\Services\Interfaces;

interface ExchangeRateServiceInterface {
    /**
     * The main function handles the exchange rate analysis request.
     * Returns the following information from user's input: exchange rate by date range, the most valuable option.
     *
     * @param array $params
     * @return array
     */
    public function analysisExchangeRates(array $params): array;
}
