<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false
]);

Route::controller(HomeController::class)
    ->middleware('auth')
    ->group(function () {
        Route::get('/', 'index')->name('main.home');
        Route::post('/analysis-exchange-rates', 'analysisExchangeRates')
            ->name('main.getCurrencyData')
            ->middleware('ajax.only');
    });
