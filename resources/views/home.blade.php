@extends('layouts.app')

@vite(['resources/js/main.js'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <p class="text-center text-primary">{!! $inspiringQuote !!}</p>

                    <p>
                        {{ __('Input date range, USD amount and click Submit to get Data') }}
                        <br>
                        {{ __('The period does not exceed 60 days') }}
                    </p>

                    <div class="row g-3">
                        <form class="row g-3" action="{{route('main.getCurrencyData')}}" id="form-get-data" novalidate>
                            <div class="col">
                                <input type="text"
                                    name="date_range"
                                    class="daterange form-control"
                                    required />
                            </div>
                            <div class="col">
                                <input
                                    class="form-control"
                                    id="amount"
                                    type="number"
                                    name="amount"
                                    min="0"
                                    placeholder="USD"
                                    required />
                                <div class="invalid-feedback">
                                    Please provide valid USD amount (integer).
                                </div>
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="spinner-ajax d-none">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Loading...
                            </div>
                        </form>
                    </div>

                    <div class="alert alert-light" role="alert" id="result-message">
                    </div>

                    <div>
                        <table class="table table-bordered table-hover d-none" id="currency-table">
                            <thead>
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">RUB</th>
                                    <th scope="col">EUR</th>
                                    <th scope="col">GBP</th>
                                    <th scope="col">JPY</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
