let ajaxPattern = (function() {

    // Private variables and functions
    let settings = {
        cache: false,
        data: '',
        dataType: 'json',
        dataTable: '',
        errorMsg: "Oops. Sorry about that.",
        form: '',
        spinner: undefined,
        retries: 0,
        success: function() {},
        beforeSend: function() {},
        type: 'POST',
        url: ''
    };

    const init = function(params) {
        // prepareParam();
        $.extend(settings, params);
        ajaxSetup();
        ajaxRequest(params);
    }

    const ajaxSetup = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    const ajaxRequest = function(params) {
        $.ajax({
            url: settings.url,
            type: settings.type,
            data: settings.form.serialize(),
            dataType: settings.dataType,
            beforeSend: function () {
                baseBeforeSend();
                settings.beforeSend();
            },
            success: settings.success,
            complete: function() {
                $( settings.spinner).hide();
                disableFormInteract(settings.form, false);
            },
            error: function( xhr, tStatus, err ) {
                if( xhr.status === 401 || xhr.status === 403 ) {
                    location.href = '/';
                } else if ( xhr.status === 504 && !settings.retries++ ) {
                    ajaxRequest();
                } else {
                    $('#result-message').addClass('text-danger').html(xhr.responseText ?? settings.errorMsg);
                }
            }
        });
    };

    const disableFormInteract = function(form, before) {
        if (before) {
            form.find("input").prop('disabled', true);
            form.find("input").prop('disabled', true);
            form.find("input").prop('disabled', true);
            form.find("input").prop('disabled', true);

            form.find(".spinner-ajax").removeClass('d-none');
        } else {
            form.find("input").prop('disabled', false);
            form.find("input").prop('disabled', false);
            form.find("input").prop('disabled', false);
            form.find("input").prop('disabled', false);

            form.find(".spinner-ajax").addClass('d-none');
        }
    }

    const baseBeforeSend = function () {
        $( settings.spinner).show();
        disableFormInteract(settings.form, true);
    }

    // Public API
    return {
        init: init
    };
})();

export default ajaxPattern;

