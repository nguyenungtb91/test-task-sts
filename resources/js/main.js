import ajaxPattern from "./ajax-common";

let MainFeature = (function() {
    // Private variables and functions
    let config = {
        form: $('#form-get-data'),
        data_table: $('#currency-table'),
        result_message: $('#result-message')
    };

    let ajaxSetting = {
        url: config.form.attr('action'),
        overlay: undefined,
        dataTable: config.data_table,
        form: config.form,
        beforeSend: function () {
            clearAndHideDataTable();
            clearMessage();
        },
        success: function (data) {
            renderMessage(data.data);
            renderTable(data.data.rates);
        }
    };

    const init = function() {
        setUpDateRangePicker();
        setUpHandleForm();
    };

    const setUpHandleForm = function() {
        config.form.submit(showData);
    };

    const showData = function(e) {
        e.preventDefault()
        e.stopPropagation()
        const isValid = config.form[0].checkValidity();
        config.form.addClass('was-validated');
        isValid && ajaxPattern.init(ajaxSetting);
    };

    const renderTable = function(data) {
        config.data_table.removeClass('d-none');
        let rowsHtml = '';
        $.each(data, function (date, value) {
            rowsHtml += `<tr>
                <td>${date}</td>
                <td>${value.RUB}</td>
                <td>${value.EUR}</td>
                <td>${value.GBP}</td>
                <td>${value.JPY}</td>
                </tr>`;
        })
        config.data_table.find('tbody').append(rowsHtml);
    }

    const renderMessage = function(data) {
        const bestChoice = data.best_choice;
        const headLine = `<p>✅ Get the results of the analysis successfully</p>`;
        const dateLine = `<p>Period: <b>${data.start_date}</b> to <b>${data.end_date}</b></p>`
        const bestChoiceLine = `<p>The best currency to buy is: <b>${bestChoice.best_currency}</b> (buy at ${bestChoice.buy_date}, sell at ${bestChoice.sell_date})</p>`;
        const formulaLine = `<p>Money you earn: <b>(${bestChoice.buy_value} * ${bestChoice.base_money} / ${bestChoice.sell_value}) - ${bestChoice.date_count} x ${bestChoice.fee_per_day} = ${bestChoice.earn_money}</b></p>`;
        const revenueLine = `<p>Revenue: <b>${bestChoice.revenue}</p></b>`;
        const resultDescriptionLine = `<p>All exchange rates below:</p>`;
        config.result_message.html(`${headLine} ${dateLine} ${bestChoiceLine} ${formulaLine} ${revenueLine} ${resultDescriptionLine}`);
    }

    const clearAndHideDataTable = function() {
        config.data_table.find('tbody').html('');
        config.data_table.addClass('d-none');
    }

    const clearMessage = function() {
        config.result_message.removeClass('text-danger').html('');
    }

    const setUpDateRangePicker = function() {
        let date = new Date();
        let today = formatDate(date);
        date.setDate(date.getDate() - 1);
        let yesterday = formatDate(date);
        $('.daterange').daterangepicker({
            startDate: yesterday,
            endDate: today,
            dateLimit: {
                'days': 60
            },
            maxDate: new Date(),
            locale: {
                format: 'YYYY-MM-DD',
                separator: " to ",
            }
        });
    };

    const formatDate = function (date) {
        return date.toISOString().split('T')[0];
    }

    // Public
    return {
        init: init
    };
})();

MainFeature.init();
